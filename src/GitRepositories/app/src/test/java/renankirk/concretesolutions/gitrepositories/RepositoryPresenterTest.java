package renankirk.concretesolutions.gitrepositories;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;

import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryBO;
import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryPRBO;
import renankirk.concretesolutions.gitrepositories.domain.dao.RepositoryDAO;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.Repository;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import renankirk.concretesolutions.gitrepositories.presenter.impl.RepositoryPresenterImpl;
import renankirk.concretesolutions.gitrepositories.view.RepositoriesView;

/**
 * Created by RenanKirk on 18/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryPresenterTest {

    private RepositoryPresenter presenter;

    @Mock
    private RepositoriesView view;

    @Mock
    private RepositoryBO repositoryBO;

    @Mock
    private RepositoryPRBO repositoryPRBO;

    @Mock
    private Repositories repositories;

    @Mock
    private RepositoryDAO dao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        presenter = new RepositoryPresenterImpl();

        Whitebox.setInternalState(presenter, "view", view);

        Whitebox.setInternalState(presenter, "repositoryBO", repositoryBO);

        Whitebox.setInternalState(presenter, "repositoryPRBO", repositoryPRBO);

        Whitebox.setInternalState(presenter, "dao", dao);
    }

    @Test
    public void showFailWhenHasNoCacheTest() {
        // Given
        String expectedMessage = "my_error_message";
        Mockito.when(dao.getSavedRepositories()).thenReturn("");

        // When
        this.presenter.failRequest(expectedMessage);

        // Then
        Mockito.verify(view).showFail(Matchers.eq(expectedMessage));
    }

    @Test
    public void showFailWhenHasCacheTest(){
        // Given
        String mockRepoJson = "{\"items\": [{\"name\": \"repo1\", \"description\": \"desc1\", \"owner\": {\"login\": \"login1\", \"avatar_url\": \"avatar1\"}, \"stargazers_count\": 10, \"forks_count\": 5 }] }";
        Mockito.doAnswer(new Answer<Void>(){
            public Void answer(InvocationOnMock invocation) {
                Repositories repos = (Repositories) invocation.getArguments()[0];
                final List<Repository> items = repos.getItems();
                Assert.assertEquals("repo1", items.get(0).getName());
                Assert.assertEquals("desc1", items.get(0).getDescription());
                Assert.assertEquals("login1", items.get(0).getOwner().getLogin());
                Assert.assertEquals("avatar1", items.get(0).getOwner().getAvatar_url());
                Assert.assertEquals(10, items.get(0).getStargazers_count());
                Assert.assertEquals(5, items.get(0).getForks_count());
                return null;
            }
        }).when(view).showRepositories(Matchers.any(Repositories.class), Matchers.anyInt());

        Mockito.when(dao.getSavedRepositories()).thenReturn(mockRepoJson);
        // When
        this.presenter.failRequest("my_error_message");
        // Then

        Mockito.verify(view).showRepositories(Matchers.any(Repositories.class), Matchers.anyInt());
    }

    @Test
    public void resquestRepositoryTest() {
        this.presenter.requestRepository(Matchers.anyInt());
        Mockito.verify(repositoryBO, Mockito.times(1)).requestRepositories(Matchers.anyInt());
    }

    @Test
    public void showRepositoriesTest() {
        this.presenter.showRepositories(repositories);
        Mockito.verify(view, Mockito.times(1)).showRepositories(Matchers.any(Repositories.class), Matchers.anyInt());
    }

    @Test
    public void requestPRRepositoryTest() {
        this.presenter.requestPRRepository("", "");
        Mockito.verify(view, Mockito.times(1)).showProgressDialog();
        Mockito.verify(repositoryPRBO, Mockito.times(1)).requestPRRepository(Matchers.anyString(), Matchers.anyString());
    }
}
