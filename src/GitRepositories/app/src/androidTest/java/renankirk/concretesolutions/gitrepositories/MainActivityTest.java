package renankirk.concretesolutions.gitrepositories;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import renankirk.concretesolutions.gitrepositories.view.activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

/**
 * Created by RenanKirk on 18/05/2016.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends ActivityInstrumentationTestCase2{

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        getActivity();
    }

    @Test
    public void showProgressTest(){
        onView(withId(R.id.rv_repositories)).check(matches(not(isDisplayed())));
        onView(withId(R.id.pb_load)).check(matches(isDisplayed()));
        onView(withId(R.id.txv_fail_conn)).check(matches(not(isDisplayed())));
    }
}
