package renankirk.concretesolutions.gitrepositories;

import android.support.test.espresso.IdlingResource;
import android.util.Log;

import java.util.List;

import renankirk.concretesolutions.gitrepositories.domain.api.RepositoryApi;
import renankirk.concretesolutions.gitrepositories.domain.bo.impl.AbstractBOImpl;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import renankirk.concretesolutions.gitrepositories.view.activity.MainActivity;
import retrofit2.Call;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RenanKirk on 19/05/2016.
 */
public class RepositoryIdlingResource implements IdlingResource, RepositoryApi {

    private ResourceCallback resourceCallback;

    private RepositoryApi api;

    private Repositories repositories;

    public RepositoryIdlingResource(RepositoryApi api){
        this.api = api;
    }

    @Override
    public String getName() {
        return RepositoryIdlingResource.class.getName();
    }

    @Override
    public boolean isIdleNow() {
        Log.d("TEST", "IDLE: " + this.repositories);
        return this.repositories != null;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        this.resourceCallback = callback;
    }

    @Override
    public Call<Repositories> loadRepositories(@Query("page") int page) {
        return null;
    }

    @Override
    public Call<RepositoryPR[]> loadRepositoryPRs(@Path("owner") String owner, @Path("repo") String repo) {
        return null;
    }
}
