package renankirk.concretesolutions.gitrepositories.presenter;

import java.util.List;

import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.Repository;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import renankirk.concretesolutions.gitrepositories.view.RepositoriesView;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public interface RepositoryPresenter {
    /**
     * request repository to bo
     * @param page
     */
    void requestRepository(int page);

    void setView(RepositoriesView view);

    /**
     * send to view to display repositories
     * @param repositories
     */
    void showRepositories(Repositories repositories);

    /**
     * requesto pull request to bo
     * @param repoName
     * @param repoOwner
     */
    void requestPRRepository(String repoName, String repoOwner);

    /**
     * send to view to display dialog pull request and save response on presenter
     * @param body
     */
    void showPRs(RepositoryPR[] body);

    /**
     * return pull request list of a repository
     * @return
     */
    List<RepositoryPR> getPRs();

    /**
     * return repository name
     * @return
     */
    String getRepoName();

    /**
     * send fail feedback to view
     * @param message
     */
    void failRequest(String message);

    /**
     * method to save data in cache
     * @param repositories
     */
    void saveCache(Repositories repositories);

    /**
     * pull request fail feedback
     * @param message
     */
    void failPRRequest(String message);
}
