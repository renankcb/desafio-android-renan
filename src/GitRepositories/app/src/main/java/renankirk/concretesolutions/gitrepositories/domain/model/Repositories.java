package renankirk.concretesolutions.gitrepositories.domain.model;

import java.util.List;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class Repositories {

    private List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
