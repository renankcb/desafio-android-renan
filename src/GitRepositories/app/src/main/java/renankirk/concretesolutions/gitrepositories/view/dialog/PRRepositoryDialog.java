package renankirk.concretesolutions.gitrepositories.view.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import renankirk.concretesolutions.gitrepositories.R;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import renankirk.concretesolutions.gitrepositories.view.activity.MainActivity;
import renankirk.concretesolutions.gitrepositories.view.adapter.RepositoryPRsAdapter;
import renankirk.concretesolutions.gitrepositories.view.adapter.RespositoryAdapter;
import roboguice.fragment.RoboDialogFragment;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class PRRepositoryDialog extends RoboDialogFragment {

    @Inject
    private RepositoryPRsAdapter adapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView mRecyclerView;

    @Inject
    private RepositoryPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_repository_pr, container, false);
        getDialog().setTitle(presenter.getRepoName());
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_repository_pr);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
        adapter.setPRs(presenter.getPRs());
        adapter.notifyDataSetChanged();
    }
}
