package renankirk.concretesolutions.gitrepositories.view.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.ArrayList;

import renankirk.concretesolutions.gitrepositories.R;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.Repository;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import renankirk.concretesolutions.gitrepositories.view.EndlessScrollRecyclerView;
import renankirk.concretesolutions.gitrepositories.view.RepositoriesView;
import renankirk.concretesolutions.gitrepositories.view.adapter.RespositoryAdapter;
import renankirk.concretesolutions.gitrepositories.view.dialog.PRRepositoryDialog;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

public class MainActivity extends RoboActionBarActivity implements RepositoriesView, View.OnClickListener {

    private static final int FIRST_PAGE = 1;
    @InjectView(R.id.pb_load)
    private ProgressBar load;

    @InjectView(R.id.txv_fail_conn)
    private TextView failInfo;

    private Dialog progressDialog;

    @Inject
    private RepositoryPresenter presenter;

    @Inject
    private RespositoryAdapter adapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView mRecyclerView;

    private PRRepositoryDialog prDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        failInfo.setOnClickListener(this);

        setupRecyclerView();

        presenter.setView(this);
        loadPage(FIRST_PAGE);
    }

    @Override
    public void showRepositories(Repositories repositories, int currentPage) {
        if (adapter.getRepositories().getItems() == null) {
            adapter.getRepositories().setItems(new ArrayList<Repository>());
        }

        //if 0 means that data come from cache
        if (currentPage == 0) {
            showToast("Dados carregados do cache, verifique sua conexão e tente recarregar.");
            adapter.setRepositories(repositories);
        } else {
            showToast("Página " + currentPage + " carregada");
            adapter.getRepositories().getItems().addAll(repositories.getItems());
            presenter.saveCache(adapter.getRepositories());
        }
        adapter.notifyDataSetChanged();

        load.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPRs() {
        progressDialog.dismiss();
        progressDialog = null;
        prDialog = new PRRepositoryDialog();
        prDialog.show(getSupportFragmentManager(), "pull_request");
        prDialog = null;
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new Dialog(MainActivity.this, R.style.full_screen_dialog);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.show();
    }

    @Override
    public void showFail(String msg) {
        if (adapter.getRepositories().getItems() != null && adapter.getRepositories().getItems().size() > 0) {
            progressDialog.dismiss();
            showToast(msg);
        } else {
            load.setVisibility(View.GONE);
            failInfo.setVisibility(View.VISIBLE);
            failInfo.setText("Falha na requisição. Clique neste texto para tentar carregar novamente");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reload) {
            adapter.getRepositories().getItems().clear();
            mRecyclerView.setVisibility(View.GONE);

            //done to reset scroll listener
            setupRecyclerView();

            load.setVisibility(View.VISIBLE);
            loadPage(FIRST_PAGE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txv_fail_conn) {
            load.setVisibility(View.VISIBLE);
            failInfo.setVisibility(View.GONE);
            loadPage(FIRST_PAGE);
        }
    }

    /**
     * bind and set adapter of recycler view
     */
    private void setupRecyclerView() {
        mLayoutManager = null;
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_repositories);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addOnScrollListener(new EndlessScrollRecyclerView((LinearLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                showToast("Carregando página: " + page);
                loadPage(page);
            }
        });
    }

    /**
     * call presenter to request repositories
     *
     * @param page
     */
    private void loadPage(int page) {
        presenter.requestRepository(page);
    }
}
