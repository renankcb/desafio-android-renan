package renankirk.concretesolutions.gitrepositories.domain.dao;

import com.google.inject.Singleton;

/**
 * Created by RenanKirk on 19/05/2016.
 */
@Singleton
public class RepositoryDAO extends AbstractDAO {

    private static final String TAG_REPOSITORY = "TAG_REPOSITORIES";

    public void saveRepositories(String repositoies){
        if(getSavedRepositories().equals("")){
            clearPreference();
        }
        getEditor().putString(TAG_REPOSITORY, repositoies).commit();
    }

    public String getSavedRepositories(){
        return getSharedPreferences().getString(TAG_REPOSITORY, "");
    }
}
