package renankirk.concretesolutions.gitrepositories.presenter.impl;

import com.google.gson.GsonBuilder;
import com.google.inject.Inject;

import java.util.Arrays;
import java.util.List;

import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryBO;
import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryPRBO;
import renankirk.concretesolutions.gitrepositories.domain.dao.RepositoryDAO;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import renankirk.concretesolutions.gitrepositories.view.RepositoriesView;
import roboguice.inject.ContextSingleton;

/**
 * Created by RenanKirk on 17/05/2016.
 */
@ContextSingleton
public class RepositoryPresenterImpl implements RepositoryPresenter {

    @Inject
    private RepositoryBO repositoryBO;

    @Inject
    private RepositoryPRBO repositoryPRBO;

    @Inject
    private RepositoryDAO dao;

    private RepositoriesView view;

    private List<RepositoryPR> prs;
    private String repoName;
    private int currentPage;

    @Override
    public void requestRepository(int page) {
        currentPage = page;
        repositoryBO.requestRepositories(page);
    }

    @Override
    public void setView(RepositoriesView view) {
        this.view = view;
    }

    @Override
    public void showRepositories(Repositories repositories) {
        view.showRepositories(repositories, currentPage);
    }

    @Override
    public void requestPRRepository(String repoName, String repoOwner) {
        this.repoName = repoName;
        view.showProgressDialog();
        repositoryPRBO.requestPRRepository(repoName, repoOwner);
    }

    @Override
    public void showPRs(RepositoryPR[] body) {
        prs = Arrays.asList(body);
        view.showPRs();
    }

    @Override
    public List<RepositoryPR> getPRs() {
        return prs;
    }

    @Override
    public String getRepoName() {
        return repoName;
    }

    @Override
    public void failRequest(String message) {
        if(dao.getSavedRepositories().equals("")){
            view.showFail(message);
        } else {
            view.showRepositories(new GsonBuilder().create().
                    fromJson(dao.getSavedRepositories(), Repositories.class), 0);
        }
    }

    @Override
    public void saveCache(Repositories repositories) {
        dao.saveRepositories(new GsonBuilder().create().toJson(repositories, Repositories.class));
    }

    @Override
    public void failPRRequest(String message) {
        view.showFail(message);
    }
}
