package renankirk.concretesolutions.gitrepositories.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import renankirk.concretesolutions.gitrepositories.R;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.Repository;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class RespositoryAdapter extends RecyclerView.Adapter<RespositoryAdapter.RepositoryViewHolder>{

    private Repositories repositories = new Repositories();

    @Inject
    private Context context;

    @Inject
    private RepositoryPresenter presenter;

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_repository_item, parent, false);
        RepositoryViewHolder vh = new RepositoryViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RepositoryViewHolder holder, int position) {
        final Repository repository = repositories.getItems().get(position);
        holder.repoOwnerName.setText(repository.getOwner().getLogin());
        holder.repoName.setText(repository.getName());
        holder.repoDesc.setText(repository.getDescription());
        holder.repoStarsCount.setText(String.valueOf(repository.getStargazers_count()));
        holder.repoForksCount.setText(String.valueOf(repository.getForks_count()));

        Glide.with(context).
                load(repository.getOwner().getAvatar_url()).
                asBitmap().
                into(holder.repoOwnerPhoto);
    }

    @Override
    public int getItemCount() {
        return repositories.getItems().size();
    }

    public Repositories getRepositories() {
        return repositories;
    }

    public void setRepositories(Repositories repositories) {
        this.repositories = repositories;
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView repoName;
        TextView repoDesc;
        TextView repoStarsCount;
        TextView repoForksCount;
        TextView repoOwnerName;
        ImageView repoOwnerPhoto;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
            repoName = (TextView) itemView.findViewById(R.id.txv_repo_name);
            repoDesc = (TextView) itemView.findViewById(R.id.txv_repo_desc);
            repoStarsCount = (TextView) itemView.findViewById(R.id.txv_stars_count);
            repoForksCount = (TextView) itemView.findViewById(R.id.txv_forks_count);
            repoOwnerName = (TextView) itemView.findViewById(R.id.txv_repo_owner_name);
            repoOwnerPhoto = (ImageView) itemView.findViewById(R.id.iv_repo_photo_owner);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            presenter.requestPRRepository(getRepositories().getItems().get(getPosition()).getName(),
                    getRepositories().getItems().get(getPosition()).getOwner().getLogin());
        }
    }
}
