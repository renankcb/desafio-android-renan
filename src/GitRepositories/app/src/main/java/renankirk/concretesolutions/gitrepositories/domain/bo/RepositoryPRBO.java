package renankirk.concretesolutions.gitrepositories.domain.bo;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public interface RepositoryPRBO {
    /**
     * request pull request to api
     * @param repoName
     * @param repoOwner
     */
    void requestPRRepository(String repoName, String repoOwner);
}
