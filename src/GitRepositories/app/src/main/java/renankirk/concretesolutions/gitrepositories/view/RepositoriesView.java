package renankirk.concretesolutions.gitrepositories.view;

import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public interface RepositoriesView {
    /**
     * show repositories list and current page loaded feedback
     * @param repositories
     * @param currentPage
     */
    void showRepositories(Repositories repositories, int currentPage);

    /**
     * show pull requests list
     */
    void showPRs();

    /**
     * show progress while load prs
     */
    void showProgressDialog();

    /**
     * show fail feedback
     * @param msg
     */
    void showFail(String msg);
}
