package renankirk.concretesolutions.gitrepositories.domain.api;

import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public interface RepositoryApi {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<Repositories> loadRepositories(@Query("page") int page);

    @GET("/repos/{owner}/{repo}/pulls")
    Call<RepositoryPR[]> loadRepositoryPRs(@Path("owner") String owner, @Path("repo") String repo);
}
