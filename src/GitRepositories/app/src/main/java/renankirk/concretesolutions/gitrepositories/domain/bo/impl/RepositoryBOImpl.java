package renankirk.concretesolutions.gitrepositories.domain.bo.impl;

import com.google.inject.Inject;

import renankirk.concretesolutions.gitrepositories.domain.api.RepositoryApi;
import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryBO;
import renankirk.concretesolutions.gitrepositories.domain.model.Repositories;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class RepositoryBOImpl extends AbstractBOImpl<Repositories> implements RepositoryBO {

    @Inject
    private RepositoryPresenter presenter;

    @Override
    public void requestRepositories(int page) {
        RepositoryApi api = getRetrofit().create(RepositoryApi.class);

        Call<Repositories> call = api.loadRepositories(page);

        call.enqueue(this);
    }

    @Override
    protected void showFail(String message) {
        presenter.failRequest(message);
    }

    @Override
    protected void showSuccess(Repositories body) {
        presenter.showRepositories(body);
    }
}
