package renankirk.concretesolutions.gitrepositories.domain.bo;

import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public interface RepositoryBO {
    /**
     * request repositories to api
     * @param page
     */
    void requestRepositories(int page);
}
