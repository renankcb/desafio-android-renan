package renankirk.concretesolutions.gitrepositories.domain.bo.impl;

import com.google.inject.Inject;

import renankirk.concretesolutions.gitrepositories.domain.api.RepositoryApi;
import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryPRBO;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import retrofit2.Call;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class RepositoryPRBOImpl extends AbstractBOImpl<RepositoryPR[]> implements RepositoryPRBO {

    @Inject
    private RepositoryPresenter presenter;

    @Override
    public void requestPRRepository(String repoName, String repoOwner) {

        RepositoryApi api = getRetrofit().create(RepositoryApi.class);

        Call<RepositoryPR[]> call = api.loadRepositoryPRs(repoOwner, repoName);

        call.enqueue(this);
    }


    @Override
    protected void showFail(String message) {
        presenter.failPRRequest(message);
    }

    @Override
    protected void showSuccess(RepositoryPR[] body) {
        presenter.showPRs(body);
    }
}
