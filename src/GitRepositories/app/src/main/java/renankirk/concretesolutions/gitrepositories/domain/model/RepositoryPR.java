package renankirk.concretesolutions.gitrepositories.domain.model;

import java.util.Date;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class RepositoryPR {

    private String html_url;

    private String title;

    private Owner user;

    private String body;

    private Date created_at;

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
